import React, {Component} from 'react';
import './header.scss';
import { Link } from 'react-router-dom';
import Logo from "../assets/images/logo.svg"; 

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      subtitle: this.props.subtitle,
      isBackButton: this.props.backButton,
      isModal: this.props.modal
    };
  }

  backButton() {
    const isBackButton = this.state.isBackButton;
    const isModal = this.state.isModal;
    if (isBackButton) {
      if(!isModal){
        return(
          <Link to="/" >
            <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4gPGc+ICA8dGl0bGU+YmFja2dyb3VuZDwvdGl0bGU+ICA8cmVjdCBmaWxsPSJub25lIiBpZD0iY2FudmFzX2JhY2tncm91bmQiIGhlaWdodD0iNDAyIiB3aWR0aD0iNTgyIiB5PSItMSIgeD0iLTEiLz4gPC9nPiA8Zz4gIDx0aXRsZT5MYXllciAxPC90aXRsZT4gIDxwb2x5Z29uIGZpbGw9IiNmZmZmZmYiIGlkPSJzdmdfMSIgcG9pbnRzPSIzNTIsMTI4LjQgMzE5LjcsOTYgMTYwLDI1NiAxNjAsMjU2IDE2MCwyNTYgMzE5LjcsNDE2IDM1MiwzODMuNiAyMjQuNywyNTYgIi8+IDwvZz48L3N2Zz4=" alt="Back" />
          </Link>
        );
      }
      else{
        return(
          <div data-dismiss="modal" aria-label="Close">
            <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4gPGc+ICA8dGl0bGU+YmFja2dyb3VuZDwvdGl0bGU+ICA8cmVjdCBmaWxsPSJub25lIiBpZD0iY2FudmFzX2JhY2tncm91bmQiIGhlaWdodD0iNDAyIiB3aWR0aD0iNTgyIiB5PSItMSIgeD0iLTEiLz4gPC9nPiA8Zz4gIDx0aXRsZT5MYXllciAxPC90aXRsZT4gIDxwb2x5Z29uIGZpbGw9IiNmZmZmZmYiIGlkPSJzdmdfMSIgcG9pbnRzPSIzNTIsMTI4LjQgMzE5LjcsOTYgMTYwLDI1NiAxNjAsMjU2IDE2MCwyNTYgMzE5LjcsNDE2IDM1MiwzODMuNiAyMjQuNywyNTYgIi8+IDwvZz48L3N2Zz4=" alt="Back" />
          </div>
        );
      }
    }
  }
  
  render() {
    return (
      <div className="header">
        <div className="row">
          <div className="col-sm-12 text-left pb-3">
            <div className={"back-buttom "+ (this.state.isBackButton ? "show" : "hide")}>
              {this.backButton()}
            </div>
            <div className="logo"><img src={Logo} alt="Share Eat" /></div>
            <h2>{this.state.title}</h2>
            <span className={this.state.isModal ? "hide" : "show"}>{this.state.subtitle}</span>
          </div>
        </div>
      </div>
    );
  }
}
  
export default Header;