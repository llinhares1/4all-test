import React, {Component} from 'react';
import './cards.scss';
import { Link } from 'react-router-dom';
import Modal from '../components/modal';

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jsonData: this.props.jsonData,
      restaurantName: '',
    }
    this.getValue = this.getValue.bind(this);
  }

  getValue(e){
    this.setState({restaurantName:e.target.attributes.getNamedItem('value').value})
  }
  
  render() {
    return (
      <div className="cards">
        <ul className="row">
          {this.state.jsonData.map((value) => (
            <li className="col-sm-12 col-md-6 mb-3" key={value.name}>
              <div className="row">
                <div className="col-10">
                  <Link to={{pathname: "/details/", state:{Data: value}}} >
                    <div className="box">
                      <div className="title">{value.name}</div>
                      <div className="qtd">{value.menuItems.length} pratos</div>
                    </div>
                  </Link>
                </div>
                <div className="col-2">
                  <div className="plus-btn m-auto" data-toggle="modal" data-target="#modal" value={value.name} onClick={this.getValue}>
                    +
                  </div>
                </div>
              </div>
            </li>
          ))}
        </ul>
        <Modal name={this.state.restaurantName} />
      </div>
    );
  }
}
  
export default Cards;