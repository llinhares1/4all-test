import React, {Component} from 'react';
import './modal.scss';
import Header from './header';
import Form from './form';

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      restaurantName: this.props.name
    };
  }
  
  render() {
    return (
      <div className="modal fade" id="modal" tabIndex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="container">
              <Header title={this.state.restaurantName} subtitle="" backButton={true} modal={true} />
              <Form />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
  
export default Modal;