import React, {Component} from 'react';
import './form.scss';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  formatPrice(e) {
    var value = e.target.value;
    value = value + '';
    value = parseInt(value.replace(/[\D]+/g,''));
    value = value + '';
    value = value.replace(/([0-9]{2})$/g, ",$1");
    console.log(e.target.value)
    if(e.target.value === "R$ " || e.target.value === "R$ Na"){
      e.target.value = "R$ 0,00";
    }
    else{
      e.target.value = "R$ "+value;
    }
  }
  
  render() {
    return (
      <form className="">
        <div className="form-row">
          <div className="form-group col-12">
            <label htmlFor="item-name">Nome do prato</label>
            <input type="text" className="form-control" id="item-name" placeholder="Prato" required />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-4 col-md-2">
            <label htmlFor="item-price">Valor</label>
            <input type="text" className="form-control" id="item-price" onChange={this.formatPrice} placeholder="R$ 0,00" required />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-12">
            <label htmlFor="item-description">Descrição do prato</label>
            <textarea className="form-control" id="item-description" rows="5" maxLength="200" placeholder="Insira uma descrição" required></textarea>
            <small id="item-description-help" >*A descrição deve conter até 200 caracteres.</small>
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-12 pt-5">
            <button type="submit" className="btn">Salvar</button>
          </div>
        </div>
      </form>
    );
  }
}
  
export default Form;