import React, {Component} from 'react';
import './cardDescription.scss';

class CardDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: this.props.menuItems
    };
  }
  
  render() {
    return (
    <div className="cards-description">
      <ul className="row">
        {this.state.menuItems.map((value) => (
          <li className="col-sm-12 col-md-6 mb-3" key={value.name}>
            <div className="row">
              <div className="col-12">
                <div className="bg-yellow">
                  <div className="box">
                    <div className="row">
                      <div className="col-8 pb-2">
                        <div className="title">{value.name}</div>
                      </div>
                      <div className="col-4 text-right pb-2">
                        <div className="title">R$ {value.price}</div>
                      </div>
                      <div className="col-12">
                        <div className="description">{value.description}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
    );
  }
}
  
export default CardDescription;