import React, {Component} from 'react';
import './details.scss';
import Header from '../components/header';
import CardDescription from '../components/cardDescription';
import PlusButtom from '../components/plusButtom';
import Modal from '../components/modal';

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  render() {
      const jsonData = this.props.location.state.Data;
      return (
        <div className="container">
          <Header title={jsonData.name} subtitle={jsonData.menuItems.length + " pratos"} backButton={true} modal={false} />
          <CardDescription menuItems={jsonData.menuItems} />
          <div data-toggle="modal" data-target="#modal">
            <PlusButtom />
          </div>
          <Modal name={jsonData.name} />
       </div>
      );
    }
  }
  
export default Details;