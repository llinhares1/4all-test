import React from 'react';
import './App.scss';
import JsonData from "../assets/data.json";
import Cards from '../components/cards';
import Header from "../components/header";


function App() {
  const json = JsonData;
  return (
    <div className="container">
      <Header title="Lugares" subtitle={json.length + " lugares cadastrados"} backButton={false} modal={false} />
      <Cards jsonData={json} />
    </div>
  );
}

export default App;
